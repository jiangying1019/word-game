﻿var state = {
    modal: { game: null },
    page: { pages: ["login", "content", "game", "gameResult"], page: null },
    user: null,
    csrf: ""
}

var meta;
var guessesLength = 0;

$(document).ready(function () {
    $.ajax({
        url: '/user',
        method: 'GET',
        success: (user) => { $('body').show().addClass('background'); setUser(user); },
        error: () => { $('body').show().addClass('background'); setPage('login'); }
    });

    $('#game').hide();
    $('#gameResult').hide();
    showModal(null);
    getFonts();
    getLevels();
    changeFont();
});

function setPage(page) {
    state.page.page = page;
    if (page == 'login') {
        $('body').addClass('background');
    } else {
        $('body').removeClass('background');
    }

    if (page == 'content') {
        retrieveGames();
    }

    state.page.pages.forEach(
        p => {
            var selector = "#" + p;
            state.page.page == p ? $(selector).show() : $(selector).hide();
        }
    );
}

function setUser(user) {
    state.user = user;
    $('#email').text(user && user.email);
    setPage(user ? 'content' : 'login');
}

function login(evt) {
    evt.preventDefault();
    var password = $('#login_password').val();
    var username = $('#login_username').val();
    if (!testEmail(username)) {
        alert('Invalid Email');
        $('#login_username').val('');
        return;
    }
    if (!testPassword(password)) {
        alert('The password must be at least 8 characters and contain at least one digit.');
        $('#login_password').val('');
        return;
    }

    $('#login_password').val('');
    $('#login_username').val('');

    $.ajax({
        url: '/wordgame/api/v2/login',
        data: { "username": username, "password": password },
        method: 'POST',
        success: function (data, status, xhr) {
            state.csrf = xhr.getResponseHeader("X_CSRF");
            setUser(data);
        },
        error: () => setUser(null)
    });
}

function testEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}

function testPassword(password) {
    var re = /\d/;
    if (password.match(re) === null || password.length < 8) {
        return false;
    }
    return true;
}

function logout(evt) {
    $.ajax({
        url: '/wordgame/api/v2/logout',
        method: 'POST',
        success: () => setUser(null)
    });
}

function changeFont() {
    $("#font").change(function () {
        var font = $("#font").val();
        for (var i = 0; i < meta.font.length; i++) {
            if (meta.font[i].family === font) {
                $('#font').css({ 'font-family': meta.font[i].rule });
                break;
            }
        }
    });
}

function showModal(game) {
    state.modal.game = game;
    if (game) {
        $('#game').slideDown();
        $('#content').slideUp();
    } else {
        $('#game').slideUp();
        $('#content').slideDown();
    }
}

function cancel() {
    letter = '';
    retrieveGames();
    showModal(null);
}

function closeResultView() {
    letter = '';
    retrieveGames();
    $('#gameResult').slideUp();
    $('#content').slideDown();
}

function showFonts(fonts) {
    for (var i = 0; i < fonts.length; i++) {
        $("#font").append("<option value=" + fonts[i].family + ">" + fonts[i].family + "</option>");
        $('<link rel="stylesheet" href='+fonts[i].url+'>').appendTo('head')
    }
}

function showLevels(metadata) {
    meta = metadata;
    for (var i = 0; i < metadata.level.length; i++) {
        $("#level").append('<option value=' + metadata.level[i].name + '>' + metadata.level[i].name + '</option>');
    }
    setDefaults(metadata);
}

function updateUserDefaults() {
    var font = $("#font").val();
    var level = $("#level").val();
    var wordColor = $("#wordColor").val();
    var guessColor = $("#guessColor").val();
    var foreColor = $("#foreColor").val();
    var defaults = getUserDefaultsObject(font, level, wordColor, guessColor, foreColor);
  
    $.ajax({
        url: `/wordgame/api/v2/${state.user.id}/defaults`,
        method: "PUT",
        headers: { X_CSRF: state.csrf },
        data: { defaults: defaults },
        success: retrieveGames,
        error: () => setUser(null)
    });
}

function getUserDefaultsObject(userFont, userLevel, wordColor, guessColor, foreColor) {
    var colors = new Colors(wordColor, guessColor, foreColor);
    var font, level;
    for (var i = 0; i < meta.font.length; i++) {
        if (meta.font[i].family === userFont) {
            font = meta.font[i];
            break;
        }
    }

    for (var j = 0; j < meta.level.length; j++) {
        if (meta.level[j].name === userLevel) {
            level = meta.level[j];
            break;
        }
    }
    var defaults = { 'colors': colors, 'level': level, 'font': font };
    return defaults; 
}

function setDefaults(metadata) {
    var font = metadata.defaults.font.family;
    var level = metadata.defaults.level.name;
    var guessColor = metadata.defaults.colors.guessColor;
    var wordColor = metadata.defaults.colors.wordColor;
    var foreColor = metadata.defaults.colors.foreColor;
    $("#level").val(level).attr("selected", true);
    $("#font").val(font).attr("selected", true);
    $("#foreColor").val(foreColor);
    $("#guessColor").val(guessColor);
    $("#wordColor").val(wordColor);
}

function Colors(wordColor, guessColor, foreColor) {
    this.wordColor = wordColor;
    this.guessColor = guessColor;
    this.foreColor = foreColor;
}

function cssStyle(game) {
    $(".guessedWord").css({
        "background-color": game.colors.guessColor,
        "color": game.colors.wordColor,
        'font-family': game.font.rule     
       });
    $(".word").css({
        "background-color": game.colors.foreColor,
        "color": game.colors.wordColor,
        'font-family': game.font.rule        
    });
}

function showGuessWords(game) {
    $("#game span").text(game.remaining + " guesses remaining. ");
    checkIfAlreadyGuessed(game);
    $(".guessedWord").remove();
    $(".word").remove();
    for (var j = 0; j < game.view.length; j++) {
        $("#rightLetter").append("<span class='word'>" + game.view.charAt(j) + "</span>");
    }

    for (var i = 0; i < game.guesses.length; i++) {
        $("#showGuessedLetters").append("<span class='guessedWord'>" + game.guesses[i] + "</span>");
    }
    cssStyle(game);
    if (game.status !== "unfinished") {
        showResult(game);
    }
    guessesLength = game.guesses.length;
}

function checkIfAlreadyGuessed(game) {
    if (letter.length > 0 && guessesLength !== 0 && guessesLength === game.guesses.length) {
        alert("Invalid guess. Already guessed.");
    }
}

//win or loss view
function showResult(game) {
    if (game.status === "loss") {
        $("#gameResult").css({ 'background-image': 'url("../pictures/cry.gif")' });
    }
    if (game.status === 'win') {
        $("#gameResult").css({ 'background-image': 'url("../pictures/winner.gif")' });
    }
    $(".word").remove();
    $(".guessedWord").remove();
   
    for (var i = 0; i < game.view.length; i++) {
        $("#answer").append("<span class='word'>" + game.view[i] + "</span>");
    }
  
    for (var j = 0; j < game.guesses.length; j++) {
        $("#guesses").append("<span class='guessedWord'>" + game.guesses[j] + "</span>");
    }
    cssStyle(game);
    $('#gameResult').slideDown();
    $('#game').slideUp();
}

function showGameView(game) {
    state.modal.game = game;
    if (game.status !== 'unfinished') {
        $('#content').slideUp();
        showResult(game);
    } else {  
            letter = '';
            showGuessWords(game);
            showModal(game);
    }
}

function makeHeader(type, values) {
   return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>`);
}

function makeRow(game) {
    var level = game.level.name;
    var guesses = game.guesses;
    var values = [level, '', game.remaining, game.target, game.status];
    var type = 'td';
    if (!game.target) {
        values[3] = '';
    }
    return $(`<tr><${type}>` + values.join(`</${type}><${type}>`) + `</${type}></${type}>`);
}

function updateGameList(games) {
    var table = $('#table').empty();
    var props = ['Level', 'Phrase', 'Remaining', 'Answer', 'Status'];
    var theNthRow = 1;
    makeHeader('th', props).appendTo(table);
    games.forEach(game => {
        var tr = makeRow(game);
        tr.click((event) => showGameView(game));
        tr.appendTo(table);
        showPhaseInRow(game, theNthRow);
        theNthRow++;
    });
};

function showPhaseInRow(game, theNthRow) {
    $('tr:last').attr('id', theNthRow);
    for (var k = 0; k < game.view.length; k++) {
        $('#' + theNthRow + ' td').eq(1).append('<span class="showPhaseInRow' + theNthRow + '">' + game.view[k]+'</span>');
    }
    cssStyleForShowPhaseInRow(game, theNthRow);
}

function cssStyleForShowPhaseInRow(game, theNthRow) {
    $('.showPhaseInRow' + theNthRow+'').css(
        {   "background-color": game.colors.foreColor,
            "color": game.colors.wordColor,
            'font-family': game.font.rule
        }
    );
}

function guessEnterEvent(event) {
    if (event.which == 13) {
        event.preventDefault();
        startGuess();
    } 
}
/************************AJAX*******************************************/
function getFonts() {
    $.ajax({
        url: "/wordgame/api/v2/meta/fonts",
        method: "GET",
        success: showFonts
    });
}

function getLevels() {
    $.ajax({
        url: "/wordgame/api/v2/meta",
        method: "GET",
        success: showLevels
    });
}

function createGame() {
    var X_font = $('#font').val();
    var level = $('#level').val();
    var wordColor = $('#wordColor').val();
    var guessColor = $('#guessColor').val();
    var foreColor = $('#foreColor').val();
    var colors = new Colors(wordColor, guessColor, foreColor);
 
    $.ajax({
        url: `wordgame/api/v2/${state.user.id}`,
        method: 'POST',
        headers: { X_CSRF: state.csrf },
        data: { level: level, colors: colors, X_font: X_font },
        success: showGameView,
        error: () => setUser(null)
    });
};

function retrieveGames() {
    $.ajax({
        url: `wordgame/api/v2/${state.user.id}`,
        method: "GET",
        headers: { X_CSRF: state.csrf },
        success: updateGameList,
        error: () => setUser(null)
    });
}

function startGuess() {
    letter = $("#guess").val().toUpperCase();
    $("#guess").val("");
    if (!letter) return;
    if (letter.length != 1) {
        alert("Invalid guess");
        return;
    }
    if (!letter.match(/[A-Z\-]/i)) {
        alert("Invalid guess");
        return;
    }
    $.ajax({
        url: `/wordgame/api/v2/${state.user.id}/${state.modal.game.id}/guesses`,
        method: "POST",
        headers: { X_CSRF: state.csrf },
        data: { guesse: letter },
        success: showGuessWords,
        error: () => setUser(null)
    });
}