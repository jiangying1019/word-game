﻿/* user is an object of type
 * { _id:       A unique id
 *   email:     A unique email address, <string>,
 *   password:  at least 8 characters and contain at least one digit,<string>,
 *   defaults:  A defaults object for this user.
 * }
 */

function User(email, password, defaults) {
    this.email = email;
    this.password = password;
    this.defaults = defaults;
}
module.exports.User = User;
