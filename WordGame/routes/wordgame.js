﻿var express = require('express');
var fs = require('fs');
var http = require("http");
var router = express.Router();
var users = require('./users');
var games = require('./games');
var settings = require('./settings');

var wordsDb = [];
var metadata;

var error = {
    success: "success",
    invalidDefaults: "invalid defaults",
    metadataIsNotAvailable: "Metadata is not available",
    fontIsNotAvailable: "Font is not available",
    initError: "Fail to initiate user"
};


function getLevelObject(level) {
    for (var i = 0; i < metadata.level.length; i++){
        if (metadata.level[i].name === level)
            return metadata.level[i];
    }
}

function getFontObject(font) {
    for (var i = 0; i < metadata.font.length; i++) {
        if (metadata.font[i].family === font)
            return metadata.font[i];
    }
}

//randomly generate a word based on the level
function generateAWord(level) {
    if (wordsDb.length === 0) {     
        initWordsDB();
    }
    var rand, minLen, maxLen;
    for (var j = 0; j < 3; j++) {
        if (metadata.level[j].name === level.name) {
            minLen = metadata.level[j].minLength;
            maxLen = metadata.level[j].maxLength;
        }   
    }
    var word = "";
    while (word.length === 0) {
        rand = Math.floor(Math.random() * wordsDb.length);
        if (wordsDb[rand].length >= minLen && wordsDb[rand].length <= maxLen)
            word = wordsDb[rand];
    }
    return word.toUpperCase();
}

function initWordsDB() {
    settings.findWords(function (err, result) {
        if (result) {
            wordsDb = result.string.split("\r\n");
        } 
    });
}

function lose(view) {
    for (var i = 0; i < view.length; i++) {
        if (view[i] === '_')
            return true;
    }
    return false;
}

function playGame(game, letter) {
    if (game.status === 'unfinished') {       
        if (game.guesses.includes(letter)) {
            return game;
        } else {
            game.guesses = game.guesses + letter;
            game.view = getView(game);
            if (game.target.includes(letter)) {
                if (!lose(game.view)) {
                    game.status = "win";
                    game.timeToComplete = Date.now();
                    return game;
                }
            } else {
                game.remaining--;
                if (game.remaining === 0 && lose(game.view)) {
                    game.status = "loss";
                    game.timeToComplete = Date.now();
                    return game;
                }   
            }
        }
    }
    else
        return game;
    return game;
}

function getView(game) {
    var array = [];
    var view = "";
    for (var m = 0; m < game.target.length; m++) {
        array[m] = "_";
    }
    for (var i = 0; i < game.guesses.length; i++) {
        for (var j = 0; j < game.target.length; j++) {
            if (game.guesses[i] === game.target[j]) {
                array[j] = game.guesses[i];
            }
        }
    }
    for (var n = 0; n < array.length; n++) {
        view = view + array[n];
    }
    return view;
}

/**********************ROUTES******************************/
router.get('/users/init', function (req, res, next) {
    if (!metadata)
        settings.findMetadata(function (err, result) {
            if (result) {
                metadata = result;
                users.init(metadata.defaults, (err, result) => res.json(result));
            } else {
                res.status.send(error.initError);
            }
        });
});

router.get('/wordgame/api/v2/meta', function (req, res, next) {
    settings.findMetadata(function (err, result) {
        if (result) {
            metadata = result;
            initWordsDB();
            res.status(200).send(result);
        } else {
            res.status.send(error.metadataIsNotAvailable);
        }
    });
});

//This endpoint delivers a list of supported fonts. 
//Response: A list of font objects.
router.get('/wordgame/api/v2/meta/fonts', function (req, res, next) {
    settings.findMetadata(function (err, result) {
        if (result) {
            fonts = result.font;
            res.status(200).send(fonts);
        } else {
            res.status.send(error.fontIsNotAvailable);
        }
    });
});

router.all('/wordgame/api/v2/:userid/*', function (req, res, next) {
    users.findById(req.params.userid, function (error, pathUser) {
        var csrf = req.get('X_CSRF');
        var authenticatedUser = req.session.user;
        var authenticatedCSRF = req.session.csrf;
        if (authenticatedUser && pathUser && authenticatedUser.id == pathUser.id && csrf === authenticatedCSRF) {
            next();
        } else {
            res.status(403).json({ msg: 'error' });
        }
    });
});

//This endpoint delivers a list of games associated with the userId.
router.get('/wordgame/api/v2/:userid', function (req, res, next) {
    games.findByOwner(req.params.userid, function (err, result) {
        res.status(200).send(result);
    });
});

//This endpoint creates a new game and associates the game with the specified sid.
router.post('/wordgame/api/v2/:userid', function (req, res, next) {
    var level = getLevelObject(req.body.level);
    var font = getFontObject(req.body.X_font);
    var colors = req.body.colors;
    var word = generateAWord(level);
    var time = Date.now();
    var guesses = "";
    var view = "";
    for (var i = 0; i < word.length; i++) {
        view = view + "_";
    }

    games.create(req.params.userid, colors, font, guesses, level, level.guesses, 'unfinished', word, time, time, view, function (err, game) {
        if (err) {
            res.status(500).send({ 'msg': 'Error creating game' });
        } else {
            res.send(game);
        }
    });   
});
//This endpoint delivers the game associated with the specified SID and having the specified game id. 
router.get('/wordgame/api/v2/:userid/:gid', function (req, res, next) {
      games.find(req.params.userid, req.params.gid, function (err, game) {
          if (game) {
            res.send(game);
        } else {
            res.status(404).send('no such game');
        }
    });
});

//This endpoint is used when a player makes a guess. 
router.post('/wordgame/api/v2/:userid/:gid/guesses', function (req, res, next) {
    games.findGame(req.params.userid, req.params.gid, function (err, game) {
        if (game) {
            playGame(game, req.body.guesse);
            games.update(req.params.userid, req.params.gid, game, function (err, newGame) {
                if (err) {
                    res.status(403).json({ msg: 'error' });
                } else {
                    res.send(newGame);
                }
            });
        } else {
            res.status(404).send('error');
        }
    });
});

router.put('/wordgame/api/v2/:userid/defaults', function (req, res, next) {
    users.updateDefaults(req.params.userid, req.body.defaults, function (err, user) {
        if (user) {
            res.send(user);
        } else {
            res.status(404).send(error.invalidDefaults);
        }
    });
});
module.exports = router;


