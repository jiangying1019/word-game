﻿/**
 * 
 * @param userId    The user id of the game creator.
 * @param colors    A colors object denoting the colors used for game-view display.
 * @param font      A font object denoting the font used for game-view display.
 * @param guesses   A string denoting all guesses the user has made in this game.
 * @param _id       A string denoting a unique id for the game.
 * @param level     A level object denoting the game level.
 * @param remaining A number denoting the number of remaining guesses.
 * @param status    A string denoting the game status. This string will be one of unfinished, loss, win.
 * @param target    A string denoting the target word. This property is only present on a completed game.
 * @param timestamp A number denoting the time of game creation.
 * @param timeToComplete A number denoting the number of milliseconds taken to complete the game. This property is only present on a completed game.
 * @param view  A string denoting the view of the target word.
 */

function Game(userId, colors, font, guesses, level, remaining, status, target, timestamp, timeToComplete, view) {
    this.userId = userId;
    this.colors = colors;
    this.font = font;
    this.guesses = guesses;
    this.level = level;
    this.remaining = remaining;
    this.status = status;
    this.target = target;
    this.timestamp = timestamp;
    this.timeToComplete = timeToComplete;
    this.view = view;
};
module.exports = Game;
