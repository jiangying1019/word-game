﻿var db = require('./db');
var mongo = require('mongodb');
var Game = require('./gameModel');
var COLLECTION = 'games';

function transformGame( game ) {
   if( game ) {
       game.id = game._id;
       if (game.status === 'unfinished') {
           delete game.target;
           delete game.timeToComplete;
       }
      delete game._id;
   }
   return game;
}

function create(userId, colors, font, guesses, level, remaining, status, target, timestamp, timeToComplete, view, cb ) {
    var result = new Game(userId, colors, font, guesses, level, remaining, status, target, timestamp, timeToComplete, view);
    db.collection(COLLECTION).insertOne( result, function( err, writeResult ) {
      cb( err, transformGame( writeResult.ops[0] ) );
    });
};
module.exports.create = create;

function findByOwner(userId, cb) {
    db.collection(COLLECTION).find({ userId: userId }).toArray(function (err, games) {
        cb(err, games.map(transformGame));
    });
}
module.exports.findByOwner = findByOwner;

function find(userId, gameId, cb) {
    db.collection(COLLECTION).findOne({ userId: userId, '_id': new mongo.ObjectID(gameId) }, function (err, game) {
        cb(err, transformGame(game));
    });
};
module.exports.find = find;

function findGame(userId, gameId, cb) {
    db.collection(COLLECTION).findOne({ userId: userId, '_id': new mongo.ObjectID(gameId) }, function (err, game) {
        cb(err, game);
    });
};
module.exports.findGame = findGame;

function update(uid, gid, newGame, cb) {
    db.collection(COLLECTION).update({ '_id': new mongo.ObjectID(gid) }, { $set: newGame }, function (err, updated) {
        find(uid, gid, cb);
    });
};
module.exports.update = update;