﻿var db = require('./db');
var User = require('./userModel');
var mongo = require('mongodb');

function transformUser(user) {
    if (user) {
        user.id = user._id;
        delete user._id;
        delete user.password;
    }
    return user;
}

function init(defaults,cb) {
    var saved = 0;
    var result = [];
    const userDb = [['bilbo@mordor.org', '123123123', defaults], ['frodo@mordor.org', '234234234', defaults], ['samwise@mordor.org', "345345345", defaults]].forEach(props => {
        var user = new User(props[0], props[1], props[2]);
        save(user, (err, newUser) => { result.push(newUser); if (result.length == 3) cb(null, result); });
    });
};
module.exports.init = init;

function save(user, cb) {
    db.collection('users').save(user, function (err1, writeResult) {
        db.collection('users').findOne(user, function (err2, savedUser) {
            cb(err1 || err2, savedUser);
        });
    });
}
module.exports.save = save;

function findByEmail(email, cb) {
    db.collection('users').findOne({ email: email }, function (err, user) {
        if (user) {
            user.id = user._id;
        }
        cb(err, user);
    });
}
module.exports.findByEmail = findByEmail;

function findById(uid, cb) {
    db.collection('users').findOne({ '_id': new mongo.ObjectID(uid) }, function (err, user) {
        cb(err, transformUser(user));
    });
}
module.exports.findById = findById;

function updateDefaults(uid, defaults, cb) {
    findById(uid, function (error, user) {
        if (error) {
            cb(error);
        } else {
            user.defaults = defaults;
            db.collection('users').update({ '_id': new mongo.ObjectID(uid) }, { $set: user }, function (err, updated) {
                findById(uid, cb);
            });
        }
    });
};
module.exports.updateDefaults = updateDefaults;