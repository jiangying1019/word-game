﻿var db = require('./db');
var mongo = require('mongodb');

function findMetadata(cb) {
    db.collection('metadata').find().toArray(function (err, metadata) {
        cb(err, metadata[0]);
    });    
}
module.exports.findMetadata = findMetadata;

function findWords(cb) {
    db.collection('words').find().toArray(function (err, words) {
        cb(err, words[0]);
    }); 
}
module.exports.findWords = findWords;
