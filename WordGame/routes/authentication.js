﻿var express = require('express');
var router = express.Router();
var users = require('./users.js');
const uuidV4 = require('uuid/v4');

router.post('/wordgame/api/v2/logout', function (req, res, next) {
    req.session.regenerate(function (err) { // create a new session id
        res.json({ msg: 'ok' });
    });
});

router.post('/wordgame/api/v2/login', function (req, res, next) {
    req.session.regenerate(function (err) {
        users.findByEmail(req.body.username, function (err, user) {
            if (user && user.password == req.body.password) {
                req.session.user = user;
                var uuid = uuidV4();
                req.session.csrf = uuid;
                res.setHeader('X_CSRF', uuid);
                res.json(user);
            } else {
                res.status(403).send('Error with username/password or status');
            }
        });
    });
});

router.get('/user', function (req, res, next) {
    var user = req.session.user;
    if (user) {
        res.json(user);
    } else {
        res.status(403).send('Forbidden');
    }
});

module.exports = router;
