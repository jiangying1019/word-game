﻿var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var routes = require('./routes/wordgame');
var authentication = require('./routes/authentication');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session(
    {
        secret: 'A SECRET KEY. SHOULD BE UNIQE TO THE APP. DONT EVER SHOW IT TO ANYONE',
        resave: true,
        saveUninitialized: true,
        cookie: { maxAge: 600000 },
        csrf: null
    }
));

app.use(express.static(path.join(__dirname, 'public')));
app.get('/wordgame', function (req, res, next) {
    res.sendFile('index.html', { root: __dirname + "/public" });
});


app.use('/', authentication);
app.use('/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.send({ msg: err.message });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send({ msg: err.message });
});

app.use(require("./routes/wordgame"));
app.listen(3000, function () {
    console.log('ready on port 3000');
});

module.exports = app;